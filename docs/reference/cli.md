---
toc: menu
---

# CLI

这里的 CLI 指的是通过 app.cli 提供的默认的 commands。

## `init`

初始化安装，支持参数有：

- `--import-demo`
- `--lang=zh-CN|en-US`

## `start`

启动 APP。

## `db:sync`

更新所有数据库结构，支持参数有：

- `--force` 删除重建

## `pm:download`

下载插件

## `pm:enable`

激活插件

## `pm:disable`

禁用插件

## `pm:remove`

移除插件
